from conans import ConanFile, CMake, tools

class V4lrtspserver( ConanFile ):
    name            = "v4lrtspserver"
    license         = ""
    url             = ""
    description     = "DDS to RTSP gateway"
    requires        = (
        ( "orovcore/1.1.0@openrov/stable" ),
        ( "orovmsg/3.6.0@openrov/stable" )
    )
    generators      = "cmake"

    # def system_requirements(self):
    #     package_tool = tools.SystemPackageTool()
    #     package_tool.install( packages="libudev-dev", update=True )
    #     package_tool.install( packages="libv4l-dev", update=True )

    def build( self ):
        cmake = CMake(self)
        # Add warnings
        # cmake.definitions["CMAKE_CXX_FLAGS"]="-Wall -Wextra -Wno-class-memaccess -Wno-expansion-to-defined"
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.package_folder + "/opt/openrov/"
        cmake.configure()
        cmake.build()

    def package( self ):
        cmake = CMake(self)
        cmake.install()

        # self.copy ("systemd/*", "lib/systemd/system", keep_path=False)
