#include <utility>


#include "trident/DDSConnection.hpp"
#include "trident/TridentVideoSource.hpp"

#include <iostream>
#include <memory>

#include <fmt/format.h>


using namespace std;

DDSConnection::DDSConnection(std::shared_ptr< Callback> callback, std::string uuid, int streamNumber)
        : m_frameCallback (std::move(callback))
        , m_ddsNode{
            "webrtc-streamer",
            {std::move(uuid)},
            {"/opt/openrov/share/orovmsg/qos/v1/openrov.xml"},
            "OpenROV",
            "Participant.Vehicle"
            }
        , m_streamNumber(streamNumber)

{
    cerr << "#### DDSConnection!! ########" <<endl;
}

DDSConnection::~DDSConnection() {
    cerr << "#### ~DDSConnection ########" <<endl;
}

void DDSConnection::start() {

    cerr << "#### DDSConnection  start ########" <<endl;
    m_stop = false;
    readerThread = thread(&DDSConnection::run, this);

}

void DDSConnection::stop() {
    m_stop = true;
    readerThread.join();
}


void DDSConnection::createDDSEntities()
{
    try
    {
        auto participant    = m_ddsNode.DefaultParticipant();
        auto publisher      = m_ddsNode.DefaultPublisher();
        auto subscriber     = m_ddsNode.DefaultSubscriber();


        // m_drVideo           = m_ddsNode.CreateReader<orov::msg::image::VideoData>("rov_cam_fwd_H264_0_video", "OpenROV::LiveVideo");
//        m_drBeacon          = m_ddsNode.CreateReader<orov::msg::system::ROVBeacon>(orov::topic::trident::rov_beacon, "OpenROV::Generic.KeepLastReliable.TransientLocal");
        m_topicVideoData    = m_ddsNode.CreateTopic<orov::msg::image::VideoData>(
                participant,
                fmt::format("rov_cam_fwd_H264_{}_video", m_streamNumber)
                );
        // Create QoS policies
        auto videoReaderQos = dds::core::QosProvider::Default()->datareader_qos( "OpenROV::LiveVideo" );
        // Create readers
        m_drVideo           = dds::sub::DataReader<orov::msg::image::VideoData>( subscriber, m_topicVideoData, videoReaderQos );


        cerr << "+++++ Created DDS " << endl;
        registerConditionHandlers();

    }
    catch(const std::exception& e)
    {
        // spdlog::critical("RecordingService: Failed to setup DDS entities: {}", e.what());
        cerr << "RecordingService: Failed to setup DDS entities: " << e.what();
        throw;
    }
}

void DDSConnection::run() {
    // cerr << "+++++ RUN " << endl;
    createDDSEntities();

    while (!m_stop) {
        // cerr << "+++++ RUN LOOP " << endl;

        auto conditions = m_waitset.wait( std::chrono::milliseconds( 100 ) );
        for( auto condition : conditions )
        {
//            cerr << "+++++ CONDITION RUN LOOP " << endl;
            if( condition == m_rcOnVideoData ){                 handler_OnVideoData(); }
            else if( condition == m_rcOnVideoNotAlive ){        handler_OnVideoNotAlive(); }
//            else if( condition == m_rcBeacon ){                handler_OnBeacon(); }
            // else if( condition == m_rcOnSessionReqNotAlive ){   Handler_OnSessionReqNotAlive(); }
        }
    }

}

void DDSConnection::registerConditionHandlers()
{
    using DataState     = dds::sub::status::DataState;
    using InstanceState = dds::sub::status::InstanceState;
    using ViewState     = dds::sub::status::ViewState;
    using SampleState   = dds::sub::status::SampleState;
    auto condNotAlive = DataState( SampleState::not_read(), ViewState::any(), InstanceState::not_alive_mask() );
    // Create read conditions
//    m_rcBeacon                  = dds::sub::cond::ReadCondition( m_drBeacon, DataState::new_data() );
    m_rcOnVideoData             = dds::sub::cond::ReadCondition( m_drVideo, DataState::new_data() );
    m_rcOnVideoNotAlive         = dds::sub::cond::ReadCondition( m_drVideo, condNotAlive );

    // Attach read conditions
//    m_waitset += m_rcBeacon;
    m_waitset += m_rcOnVideoData;
    m_waitset += m_rcOnVideoNotAlive;
    cerr << "+++++ registered conditions DDS " << endl;
}

void DDSConnection::handler_OnVideoNotAlive()  {
    // Take new samples
    auto samples = m_drVideo.select()
            .condition( m_rcOnVideoNotAlive )
            .take();

    for( auto sample : samples )
    {
        if( !sample.info().valid() )
        {
            cerr << "No writers alive for video stream! Stopping any active recordings." << endl;
            // m_recorder.CreateSessionRequest( SessionRequest(
            //     "",
            //     "",
            //     false,
            //     true,
            //     std::function<void(const std::string, SessionRequest::EResponse)>(nullptr) ) );
        }
    }
}

void DDSConnection::handler_OnVideoData()
{
    // Take new data samples
    auto samples = m_drVideo.select()
            .condition( m_rcOnVideoData )
            .take();

    // Send them to the recorder
    for( auto sample : samples )
    {
        if( sample.info().valid() )
        {
            // Create frame
            auto data = sample.data().data().data();
            auto size = sample.data().data().size();

            timeval tv = timeval();
            gettimeofday(&tv, NULL);

            auto frame = make_shared<orov::h264::TFrame>(
                    data,
                    size,
                    orov::h264::IsKeyFrame(sample.data().data().data(), size),
                    tv
            );


            try
            {
                m_frameCallback->onFrame( frame );
//                for(const auto& clbk : frameHandlers) {
//                    clbk(frame);
//                }
//                m_callback( std::move( frame ));
                // m_recorder.WriteFrame( frame );
            }
            catch( const std::exception& e )
            {
                // Non-fatal error. The recorder is responsible for handling frame gaps
                // SPDLOG_DEBUG( m_logger, "Failed to write frame: {}", e.what() );
                cerr <<  "Failed to write frame: " << e.what() << endl;
            }
        }
    }
}