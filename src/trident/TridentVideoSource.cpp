#include <utility>

#include "logger.h"
#include "trident/TridentVideoSource.hpp"
#include <Base64.hh>


TridentVideoSource::TridentVideoSource(string tridentUuid, int streamNum, int queueSize, bool repeatConfig, UsageEnvironment& env)
    : StreamnigDeviceSource(env)
    , m_repeatConfig(repeatConfig)
    , m_queueSize(queueSize)

{
    auto self = std::shared_ptr<DDSConnection::Callback>(this);
    m_eventTriggerId = envir().taskScheduler().createEventTrigger(TridentVideoSource::deliverFrameStub);
    auto connection =
        new DDSConnection(
            self
            , std::move(tridentUuid)
            , streamNum
    );
    m_connection = std::unique_ptr<DDSConnection>(connection);

    memset(&m_thid, 0, sizeof(m_thid));
    memset(&m_mutex, 0, sizeof(m_mutex));
    pthread_mutex_init(&m_mutex, NULL);
    m_connection->start();
//    pthread_create(&m_thid, NULL, threadStub, this);


    if (streamNum == 1) {
        m_width = 1080;
        m_height = 720;
    } else if (streamNum == 0) {
        m_width = 1920;
        m_height = 1080;

    }
}

TridentVideoSource::~TridentVideoSource()
{
    envir().taskScheduler().deleteEventTrigger(m_eventTriggerId);
    pthread_join(m_thid, NULL);
    pthread_mutex_destroy(&m_mutex);

    m_connection->stop();
    m_connection = nullptr;
}

// thread mainloop
void* TridentVideoSource::threadLoop()
{
    int stop=0;
    fd_set fdset;
    FD_ZERO(&fdset);

//    m_connection->start();

    LOG(NOTICE) << "begin thread";
    while (!stop)
    {
        if (this->getNextFrame() <= 0)
        {
            LOG(ERROR) << "error:" << strerror(errno);
            stop=1;
        }
    }
    m_connection->stop();
    LOG(NOTICE) << "end thread";
    return NULL;
}

void TridentVideoSource::onFrame(const std::shared_ptr<orov::h264::TFrame> frame) {

    timeval tv;
    gettimeofday(&tv, NULL);

    processFrame(frame, tv);
    //    pthread_mutex_lock(&m_mutex);
//    m_captureQueue.push_back(frame);
//    pthread_mutex_unlock(&m_mutex);
//    envir().taskScheduler().triggerEvent(m_eventTriggerId, this);
//    LOG(INFO) << ".";
//    std::cerr << "." << endl;
}

std::list< std::shared_ptr<orov::h264::TFrame> > TridentVideoSource::splitFrames(std::shared_ptr<orov::h264::TFrame> frame) {
    std::list< std::shared_ptr<orov::h264::TFrame> > frameList;

    size_t bufSize = frame->length;
    size_t size = 0;
    int frameType = 0;
    unsigned char* buffer = this->extractFrame( (unsigned char*) frame->data, bufSize, size, frameType);
    while (buffer != NULL)
    {
        switch (frameType&0x1F)
        {
            case 7: LOG(INFO) << "SPS size:" << size << " bufSize:" << bufSize; m_sps.assign((char*)buffer,size); break;
            case 8: LOG(INFO) << "PPS size:" << size << " bufSize:" << bufSize; m_pps.assign((char*)buffer,size); break;
            case 5: LOG(INFO) << "IDR size:" << size << " bufSize:" << bufSize;
                if (m_repeatConfig && !m_sps.empty() && !m_pps.empty())
                {
                    auto spsFrame = std::make_shared< orov::h264::TFrame >((unsigned char*)m_sps.c_str(),  m_sps.size(), false, frame->timestamp);
                    auto ppsFrame = std::make_shared< orov::h264::TFrame >((unsigned char*)m_pps.c_str(),  m_pps.size(), false, frame->timestamp);
                    frameList.push_back(spsFrame);
                    frameList.push_back(ppsFrame);
                }
                break;
            default:
                break;
        }

        if (!m_sps.empty() && !m_pps.empty())
        {
            u_int32_t profile_level_id = 0;
            if (m_sps.size() >= 4) profile_level_id = (m_sps[1]<<16)|(m_sps[2]<<8)|m_sps[3];

            char* sps_base64 = base64Encode(m_sps.c_str(), m_sps.size());
            char* pps_base64 = base64Encode(m_pps.c_str(), m_pps.size());

            std::ostringstream os;
            os << "profile-level-id=" << std::hex << std::setw(6) << std::setfill('0') << profile_level_id;
            os << ";sprop-parameter-sets=" << sps_base64 <<"," << pps_base64;
            m_auxLine.assign(os.str());

            delete [] sps_base64;
            delete [] pps_base64;
        }
        auto newFrame = std::make_shared< orov::h264::TFrame >(buffer, size, false, frame->timestamp);
        frameList.push_back( newFrame );

        buffer = this->extractFrame(&buffer[size], bufSize, size, frameType);
    }
    return frameList;

}

void TridentVideoSource::processFrame(std::shared_ptr<orov::h264::TFrame> frame, const timeval &ref)
{
    timeval tv;
    gettimeofday(&tv, NULL);
    timeval diff;
    timersub(&tv,&ref,&diff);

    auto frameList = this->splitFrames(frame);
    while (!frameList.empty())
    {
        auto& item = frameList.front();
//        size_t size = frame->length;
//        char* buf = new char[size];
//        memcpy(buf, frame->data, size);
//        auto newFrame

        queueFrame(item,ref);

        LOG(DEBUG) << "queueFrame\ttimestamp:" << ref.tv_sec << "." << ref.tv_usec << "\tsize:" << item->length <<"\tdiff:" <<  (diff.tv_sec*1000+diff.tv_usec/1000) << "ms";
        frameList.pop_front();
    }
}

void TridentVideoSource::queueFrame(std::shared_ptr<orov::h264::TFrame> frame, const timeval &tv)
{
    pthread_mutex_lock (&m_mutex);
    while (m_captureQueue.size() >= m_queueSize)
    {
        LOG(DEBUG) << "Queue full size drop frame size:"  << (int)m_captureQueue.size() ;
        m_captureQueue.pop_front();
    }
    m_captureQueue.push_back(frame);
    pthread_mutex_unlock (&m_mutex);

    // post an event to ask to deliver the frame
    envir().taskScheduler().triggerEvent(m_eventTriggerId, this);
}



void TridentVideoSource::doGetNextFrame() {
    deliverFrame();
}

void TridentVideoSource::doStopGettingFrames() {
    LOG(NOTICE) << "TridentVideoSource::doStopGettingFrames";
    FramedSource::doStopGettingFrames();
}

void TridentVideoSource::incomingPacketHandler() {

    LOG(NOTICE) << "TridentVideoSource::incomingPacketHandler" << endl;
    /// TODO
    //    if (this->getNextFrame() <= 0)
//    {
//        handleClosure(this);
//    }

}


void TridentVideoSource::deliverFrame() {
    if (isCurrentlyAwaitingData()) {
        fDurationInMicroseconds = 0;
        fFrameSize = 0;

        pthread_mutex_lock(&m_mutex);
        if (m_captureQueue.empty()) {

            LOG(DEBUG) << "Queue is empty";
        } else {
            timeval curTime = timeval();
            gettimeofday(&curTime, NULL);
            auto frame = m_captureQueue.front();
            m_captureQueue.pop_front();

//            m_out.notify(curTime.tv_sec, frame->m_size);
            if (frame->length > fMaxSize) {
                fFrameSize = fMaxSize;
                fNumTruncatedBytes = frame->length - fMaxSize;
            } else {
                fFrameSize = frame->length;
            }
            timeval diff = timeval();
            timeval tstamp =frame->timestamp;
            timersub(&curTime, &(tstamp), &diff);

            LOG(DEBUG) << "deliverFrame\ttimestamp:" << curTime.tv_sec << "." << curTime.tv_usec << "\tsize:"
                       << fFrameSize << "\tdiff:" << (diff.tv_sec * 1000 + diff.tv_usec / 1000) << "ms\tqueue:"
                       << m_captureQueue.size();

            fPresentationTime = tstamp;
            memcpy(fTo, frame->data, fFrameSize);
            frame = nullptr;
        }
        pthread_mutex_unlock(&m_mutex);

        if (fFrameSize > 0) {
            // send Frame to the consumer
            FramedSource::afterGetting(this);
        }
    }
}

// extract a frame
unsigned char*  TridentVideoSource::extractFrame(unsigned char* frame, size_t& size, size_t& outsize, int& frameType)
{
    unsigned char * outFrame = NULL;
    outsize = 0;
    unsigned int markerlength = 0;
    frameType = 0;

    unsigned char *startFrame = (unsigned char*)memmem(frame,size, orov::h264::H264marker,sizeof(orov::h264::H264marker));
    if (startFrame != NULL) {
        markerlength = sizeof(orov::h264::H264marker);
    } else {
        startFrame = (unsigned char*)memmem(frame,size,orov::h264::H264shortmarker,sizeof(orov::h264::H264shortmarker));
        if (startFrame != NULL) {
            markerlength = sizeof(orov::h264::H264shortmarker);
        }
    }
    if (startFrame != NULL) {
        frameType = startFrame[markerlength];

        int remainingSize = size-(startFrame-frame+markerlength);
        unsigned char *endFrame = (unsigned char*)memmem(&startFrame[markerlength], remainingSize, orov::h264::H264marker, sizeof(orov::h264::H264marker));
        if (endFrame == NULL) {
            endFrame = (unsigned char*)memmem(&startFrame[markerlength], remainingSize, orov::h264::H264shortmarker, sizeof(orov::h264::H264shortmarker));
        }

        if (m_keepMarker)
        {
            size -=  startFrame-frame;
            outFrame = startFrame;
        }
        else
        {
            size -=  startFrame-frame+markerlength;
            outFrame = &startFrame[markerlength];
        }

        if (endFrame != NULL)
        {
            outsize = endFrame - outFrame;
        }
        else
        {
            outsize = size;
        }
        size -= outsize;
    } else if (size>= sizeof(orov::h264::H264shortmarker)) {
        LOG(INFO) << "No marker found";
    }

    return outFrame;
}


int TridentVideoSource::getNextFrame() {
    return 1;
}

