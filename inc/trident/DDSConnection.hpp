#pragma once

#include <string.h>
#include <functional>
#include <vector>
#include <queue>
#include <thread>
#include <memory>
#include <mutex>
#include <iostream>
#include <condition_variable>

#include "trident/H264Util.hpp"

// DDS
#include <dds/dds.hpp>
// Orovcore
#include <orov/core/Node.hpp>
#include <orov/msg/system/ROVBeacon.hpp>
#include <orov/msg/image/Data.hpp>
#include <orov/msg/recording/Recording.hpp>
#include <orov/topic/Trident.hpp>

typedef std::function<void(std::shared_ptr<orov::h264::TFrame>)> FrameHandler;

class DDSConnection {

public:

    class Callback
    {
    public:
        virtual void    onFrame(const std::shared_ptr<orov::h264::TFrame>) {}
    };

    DDSConnection(std::shared_ptr< Callback> callback, std::string uuid, int streamNumber);
    ~DDSConnection();

    void start();
    void stop();

private:
    void createDDSEntities();
    void registerConditionHandlers();
    void handler_OnVideoData();
    void handler_OnVideoNotAlive();
//    void handler_OnBeacon();
    void run();

    // Frame handling
    std::shared_ptr< Callback> m_frameCallback;


    // DDS entities
    orov::core::Node m_ddsNode;

    // Waitset and conditions
    dds::core::cond::WaitSet        m_waitset;
//    dds::sub::cond::ReadCondition   m_rcBeacon             = nullptr;  // DataState::new_data()
    dds::sub::cond::ReadCondition   m_rcOnVideoData             = nullptr;  // DataState::new_data()
    dds::sub::cond::ReadCondition   m_rcOnVideoNotAlive         = nullptr;  // DataState( InstanceState::not_alive_mask(), ViewState::any(), InstanceState::any())

    // Topics
    dds::topic::Topic<orov::msg::image::VideoData>                   m_topicVideoData    = nullptr;

    // Readers
//    dds::sub::DataReader<orov::msg::system::ROVBeacon>               m_drBeacon          = nullptr;
    dds::sub::DataReader<orov::msg::image::VideoData>                m_drVideo           = nullptr;

    bool m_stop = false;
    int m_streamNumber;
    std::thread readerThread;

};