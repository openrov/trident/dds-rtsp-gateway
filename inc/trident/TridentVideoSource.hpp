#pragma once

#include <string.h>
#include <functional>
#include <list>
#include <vector>
#include <queue>
#include <thread>
#include <memory>
#include <mutex>
#include <iostream>
#include <condition_variable>

#include <DeviceSource.h>

#include "H264Util.hpp"

#include "trident/DDSConnection.hpp"

using namespace std;

class TridentVideoSource
        : public DDSConnection::Callback
        , public StreamnigDeviceSource
        , public std::enable_shared_from_this<TridentVideoSource>
        {

public:
    TridentVideoSource(string tridentUuid, int streamNum, int queueSize, bool repeatConfig, UsageEnvironment& env);
    ~TridentVideoSource();
    virtual std::string getAuxLine() override { return m_auxLine; }
    virtual int getWidth() override { return m_width; };
    virtual int getHeight() override { return m_height; };


protected:
    int getNextFrame();

    void processFrame(std::shared_ptr<orov::h264::TFrame>, const timeval &ref);

    void queueFrame(std::shared_ptr<orov::h264::TFrame>, const timeval &tv);

    virtual std::list< std::shared_ptr<orov::h264::TFrame> > splitFrames(std::shared_ptr<orov::h264::TFrame> frame);

    // overide FramedSource
    virtual void doGetNextFrame();
    virtual void doStopGettingFrames();

    void incomingPacketHandler();

    // DDSConnection::Callback
    virtual void onFrame(const std::shared_ptr<orov::h264::TFrame>);

private:
    static void deliverFrameStub(void* clientData) {((TridentVideoSource*) clientData)->deliverFrame();};
    void deliverFrame();
    static void* threadStub(void* clientData) { return ((TridentVideoSource*) clientData)->threadLoop();};
    void* threadLoop();

    unsigned char* extractFrame(unsigned char* frame, size_t& size, size_t& outsize, int& frameType);

    std::string m_sps;
    std::string m_pps;
    bool        m_repeatConfig;
    bool        m_keepMarker = false;
    std::string m_auxLine;
    int         m_width;
    int         m_height;
    EventTriggerId m_eventTriggerId;

    pthread_t       m_thid;
    pthread_mutex_t m_mutex;

    size_t m_queueSize;
    std::unique_ptr<DDSConnection> m_connection;
    std::list<std::shared_ptr<orov::h264::TFrame>> m_captureQueue;
};