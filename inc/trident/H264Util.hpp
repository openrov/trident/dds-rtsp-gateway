#pragma once
#include <cstring>
namespace orov {
    namespace h264 {

        const char H264marker[] = {0,0,0,1};
        const char H264shortmarker[] = {0,0,1};

        class TFrame
        {
        public:
            TFrame(const uint8_t* _data, size_t _length, bool _isKey, timeval _timestamp) {
                data = new uint8_t[_length];
                length = _length;
                isKey = _isKey;
                timestamp = _timestamp;
                std::memcpy((void*)data, _data, length);

            }

            ~TFrame() {
                if (data != NULL)
                {
                    delete data;
                    data = NULL;
                }
            }

            // View into an existing buffer. In this implementation, we will not do any additional dynamic allocation.
            const uint8_t* data;
            size_t length;
            bool isKey;
            timeval timestamp;


        };

        inline bool IsKeyFrame( const uint8_t* buffer, int length)
        {
            long data, type;
            long status = 0;
            int offset = 0;

            while (length > 0) {
                data = buffer[offset];
                offset++;
                length--;

                switch (status) {
                    case 0:
                    case 1:
                    case 2:
                        if (data == 0x00) { // NAL Header first 3 byte = "0x00 00 00"
                            status++;
                        } else {
                            status = 0;
                        }
                        break;

                    case 3:
                        if (data == 0x01) { // NAL Header 4th byte = '0x01'
                            status++;
                        } else {
                            status = 0;
                        }
                        break;

                    case 4: // 1st byte of NAL Header
                        type = data & 0x1F;

                        if (type == 1) { //check if this is non-IDR frame
                            return false;
                        } else if (type == 5 || type == 7 || type == 8 ) { //Check if this is IDR frame
                            return true;
                        } else {
                            if (type == 20 || type == 14) {
                                status++; // To Next byte
                            } else {
                                status = 0;
                            }
                        }
                        break;

                    default:
                        return false;
                }
            }

            return false;
        }

    } // h264
} // orov